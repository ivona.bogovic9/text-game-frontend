import axios from 'axios';

const API_URL = 'http://localhost:8085';

class AuthService {
    login(user) {
        return axios
            .post(API_URL + '/authenticate', {
                username: user.username,
                password: user.password
            })
            .then(response => {
                let return_obj = {}
                if (response.data.jwt) {
                    axios.get(API_URL + '/users/' + user.username, {
                            headers: {
                                'Authorization': `Bearer ${response.data.jwt}`
                            },
                        })
                        .then(response => {
                            return_obj = response.data;
                        })
                }
                // return_obj['username'] = user.username;
                return_obj['jwt'] = response.data['jwt'];
                localStorage.setItem('user', JSON.stringify(return_obj));
                console.log(return_obj['jwt']);
                return return_obj;
            });
    }

    logout() {
        localStorage.removeItem('user');
        //localStorage.setItem('username', jwt.sub)
    }

    register(user) {
        return axios.post(API_URL + '/register', {
            username: user.username,
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();